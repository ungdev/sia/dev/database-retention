# Database retention

This script is primarly used for database retention

## What does it do

```
database-retention SOURCE DESTINATION
```

Imagine you have the following tree for the source

```
source
└── mysql
    └── pA
        ├── backup-2019-09-25_03-00.sql.gz
        └── backup-2019-09-25_04-00.sql.gz
tmp
```

After one execution of the main loop, the script looks like this

```
source
└── mysql
    └── pA
        ├── backup-2019-09-25_03-00.sql.gz
tmp
└── mysql
    └── pA
        └── backup.sql
```

Then, from the tmp file, the script create a borg repository of pA to the destination file, respecting the folder tree

```
dest
└── mysql
    └── pA (borg repo)
        └── backup.sql
```

And prunes the old backups
