#!/bin/bash -e

if [[ "$#" != 3 ]]; then
  echo "Invalid number of arguments. Expected script <source> <tmp> <destination>"
  exit 1
fi

# Remove eventual ./ before folder
source=$(echo $1 | sed 's/\.\///')
tmp=$(echo $2 | sed 's/\.\///')
destination=$(echo $3 | sed 's/\.\///')

# Find only leaf directories
for leaf in $(find $source -type d -exec sh -c '(ls -p "{}"|grep />/dev/null)||echo "{}"' \;); do
  
  # Format ($source/filepath)
  oldest_file=$(ls -t1rd $leaf/* 2> /dev/null | head -n 1)

  while [[ -f $oldest_file ]]; do

    # File format : name-YYYY-MM-dd_HH-mm.ext
    file=${oldest_file##*/}

    # Transforms to format YYYY-MM-ddTHH:mm:00
    timestamp=$(echo $file | sed -nr 's/^.*-([0-9]{4}-[0-9]{2}-[0-9]{2})_([0-9]{2})-([0-9]{2})[\.a-z]+$/\1T\2:\3:00/p')
    
    # Transforms to format name.ext
    destination_filename=$(echo $file | sed -nr 's/^(.*)-[0-9]{4}-[0-9]{2}-[0-9]{2}_[0-9]{2}-[0-9]{2}([\.a-z]+)$/\1\2/p')

    # Create the destination file path from the oldest_file, remove source . Uses ~ because there are /
    # Remove the file from the path. More information on https://www.linuxjournal.com/article/8919
    relative_directory=$(echo ${oldest_file%/*} | sed "s~^$source/~~")

    # Moves the oldest file to the tmp directory
    mkdir -vp $tmp/$relative_directory
    mv -v $oldest_file $tmp/$relative_directory/$destination_filename
    gunzip -f $tmp/$relative_directory/$destination_filename # Do not use destination_filename after this line

    # Initiates borg directory
    mkdir -vp $destination/$relative_directory
    
    borg init --encryption=none $destination/$relative_directory 2> /dev/null || true # Ignore the error if the repo already exists
  
    borg create -s --timestamp $timestamp $destination/$relative_directory::$timestamp $tmp/$relative_directory
    
    # Keeps all backups in 2 days, 14 daily backups, 6 weekly backup, one backup every month
    borg prune -s --list --keep-within 2d -d 14 -w 6 -m -1 $destination/$relative_directory

    # Refreshes the oldest file for the while loop
    # Format ($source/filepath) 
    oldest_file=$(ls -t1rd $leaf/* 2> /dev/null | head -n 1)
  done
  rmdir $leaf
done
